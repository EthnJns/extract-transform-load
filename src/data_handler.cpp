#include "../headerfiles/data_handler.h"
#include <iostream>
#include <fstream>
Data_Handler::Data_Handler(){
	data_array = new std::vector<Data*>;
	test_data = new std::vector<Data*>;
	training_data = new std::vector<Data*>;
	validation_data = new std::vector<Data*>;
}

Data_Handler::~Data_Handler(){
//Free memory
}
  

void Data_Handler::read_feature_vector(std::string path){
	uint32_t header[4]; //[MAGIC][NUM_IMAGES][ROWSIZE][COLSIZE]
	unsigned char bytes[4];
	FILE *f = fopen(path.c_str(), "r");
	if(f){
	  for(int i = 0; i < 4; i++){
	    if(fread(bytes,sizeof(bytes),1,f)){
		header[i] = convert_to_little_endian(bytes);
	    }
	  }
	  
	  printf("Done Getting File Header\n");
	  int image_size = header[2]*header[3];
	  for(int i = 0; i < header[1]; i++){
	    Data* d = new Data();
	    uint8_t element[1];
	    for(int j = 0; j < image_size; j++){
		if(fread(element, sizeof(element),1,f)){
		  d->append_to_feature_vector(element[0]);
		}
		else{
		  printf("Error reading from file.\n");
		  exit(1);
		}
	    }
	    data_array->push_back(d);
	  }
	  printf("Successfully  read and stored %lu feature vectors.\n", data_array->size());
	}
	else{
	    printf("Could not find file.\n");
	    exit(1);
	}

}

void Data_Handler::read_feature_labels(std::string path){
	uint32_t header[4]; //[MAGIC][NUM_IMAGES]
	unsigned char bytes[4];

	FILE *f = fopen(path.c_str(), "r");
	if(f){
	  for(int i = 0; i < 4; i++){
	    if(fread(bytes,sizeof(bytes),1,f) == 1){
		header[i] = convert_to_little_endian(bytes);
	    }
	  }
	  printf("Done Getting Label File Header\n");
	  for(int i = 0; i < header[1]; i++){
	    Data* d = new Data();
	    uint8_t element[1];
	    int check = fread(element, sizeof(element),1,f);
	    if(check == 1){
		  data_array->at(i)->set_label(element[0]);
		  //printf("%d:%d\n",i, element[0]);
		}
		else{
		  if(check == 0) // Reached end of file for some reason
			break;
		  printf("Error reading from file.\n");
		  exit(1);
		}
	    }
	  printf("Successfully  read and stored labels\n");
	}
	else{
	    printf("Could not fine file.\n");
	    exit(1);
	}
}

void Data_Handler::split_data(){
	std::unordered_set<int> used_indexes;
	int train_size = data_array->size() * TRAIN_SET_PERCENT;
	int test_size  = data_array->size() * TEST_SET_PERCENT;
	int valid_size = data_array->size() * VALIDATION_PERCENT;

	//Training Data
	int count = 0;
	while(count < train_size){
		int rand_index = rand()% data_array->size();
		if(used_indexes.find(rand_index) == used_indexes.end()){
			training_data->push_back(data_array->at(rand_index));

			used_indexes.insert(rand_index);
			count++;
		}
	}
 	count = 0;
	//Test Data
	while(count < test_size){
		int rand_index = rand()% data_array->size();
		if(used_indexes.find(rand_index) == used_indexes.end()){
			test_data->push_back(data_array->at(rand_index));
			used_indexes.insert(rand_index);
			count++;
		}
	}
	count = 0;
	//Validation Data
	while(count < valid_size){
		int rand_index = rand()% data_array->size();
		if(used_indexes.find(rand_index) == used_indexes.end()){
			validation_data->push_back(data_array->at(rand_index));
			used_indexes.insert(rand_index);
			count++;
		}
	}
	printf("Training Data Size: %lu\n", training_data->size());
	printf("Test Data Size: %lu\n", test_data->size());
	printf("Validation Data Size: %lu\n", validation_data->size());
}
void Data_Handler::count_classes(){
	int count = 0;
	for (unsigned i = 0; i < data_array->size(); i++){
	  if(class_map.find(data_array->at(i)->get_label()) == class_map.end()){
	    class_map[data_array->at(i)->get_label()] = count;
	    data_array->at(i)->set_enumerated_label(count);
	    count++;
	  }
	}
	num_classes = count;
	printf("Successfully Extracted %d Unique Classes\n", num_classes);
}

uint32_t Data_Handler::convert_to_little_endian(const unsigned char* bytes){
	return (uint32_t)((bytes[0] << 24) |
			  (bytes[1] << 16) |
			  (bytes[2] << 8)  |
			  (bytes[3]));
}

std::vector<Data*> * Data_Handler::get_training_data(){
	return training_data;
}
std::vector<Data*> * Data_Handler::get_test_data(){
	return test_data;
}
std::vector<Data*> * Data_Handler::get_validation_data(){
	return validation_data;
}

int main(){
	Data_Handler *dh = new Data_Handler();
	dh->read_feature_vector("/home/Ethan/Documents/programming/C++/ai/etl/src/train-images-idx3-ubyte");
	dh->read_feature_labels("/home/Ethan/Documents/programming/C++/ai/etl/src/train-labels-idx1-ubyte");
	dh->split_data();
	dh->count_classes();

}
