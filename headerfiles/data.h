#ifndef __DATA_H
#define __DATA_H

#include <vector>
#include "stdint.h"
#include "stdio.h"

class Data{
private:
  
  std::vector<uint8_t> * feature_vector; //everythin but the class
  uint8_t label;
  int enumerated_label; //A-> 1, B ->2, ...just an example

public:
  Data();
  ~Data();
  void set_feature_vector(std::vector<uint8_t> *);
  void append_to_feature_vector(uint8_t);
  void set_label(uint8_t);
  void set_enumerated_label(int);

  int get_feature_vector_size();
  uint8_t get_label();
  uint8_t get_enumerated_label();

  std::vector<uint8_t>* get_feature_vector();
};

#endif
