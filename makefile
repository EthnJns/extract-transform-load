CC=g++
SRC=./src

main: $(SRC)/data_handler.o $(SRC)/data.o
	$(CC) $(SRC)/data_handler.o $(SRC)/data.o -o main

data_handler.o: data_handler.cpp
	$(CC) -c $(SRC)/data_handler.cpp

data.o: data.cpp
	$(CC) -c $(SRC)/data.cpp

clean:
	rm $(SRC)/*.o
	
